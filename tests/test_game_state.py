import pytest
import hypothesis
import hypothesis.strategies as st
import numpy as np

from backgammon.board import Board
from backgammon.game_state import GameState

@pytest.fixture
def player_0_start():
    return GameState.start(start_player = 0)

def _player_checkers(player_points):
    num_occupied = len(player_points)
    if not num_occupied:
        return np.array([], dtype=np.uint8), np.random.randint(3)
    num_checkers = np.random.randint(num_occupied, 16)
    max_checkers_bar = min(3, 15-num_checkers)
    num_checkers_bar = np.random.randint(max_checkers_bar+1)
    split_indices = sorted(np.random.choice(np.arange(num_checkers-1), num_occupied-1, replace=False))
    counts = np.diff(split_indices, prepend=-1, append=num_checkers-1)
    assert(sum(counts) == num_checkers)
    assert(num_checkers + num_checkers_bar <= 15)
    return counts, num_checkers_bar


def _board(num_occupied_points):
    occupied_points = np.random.choice(np.arange(24), num_occupied_points, replace=False)
    if num_occupied_points:
        player_occupied_points = np.split(occupied_points, np.array([np.random.randint(num_occupied_points)]))
    else:
        player_occupied_points = [np.array([], dtype = np.uint8), np.array([], dtype = np.uint8)]

    player_checkers_points_bar = [_player_checkers(points) for points in player_occupied_points]
    
    points_array = np.zeros((2,24), dtype = np.uint8)
    bar = np.zeros((2,), dtype=np.uint8)
    
    for i, (points, (checkers_points, checkers_bar)) in enumerate(zip(player_occupied_points, player_checkers_points_bar)):
        for point, checker_count in zip(points, checkers_points):
            points_array[i, point] = checker_count
        bar[i] = checkers_bar   

    return Board(
        points = points_array,
        bar = bar
    )

board_strategy = st.builds(
    _board,
     st.integers(min_value=0, max_value=10)
)


def test_nobody_has_won_at_start(player_0_start):
    assert player_0_start.has_won() == (False, None)

def test_next_boards_from_rolling_1_at_start(player_0_start):
    next_boards = player_0_start._get_next_boards(
        board = player_0_start.board,
        roll = 1
    )
    print(next_boards)

@hypothesis.given(board=board_strategy)
@hypothesis.settings(max_examples=5000)
def test_hypothesis(board):
    board.pprint()
    print(board.bar)
    
