import pytest

import numpy as np

from backgammon.board import Board

@pytest.fixture
def start_board():
    return Board.start()

def test_instantiation(start_board):
    assert start_board.points.shape == (2, 24)

def test_boards_equal_when_points_and_bar_equal(start_board):
    board_2 = Board.start()
    
    assert start_board == board_2

def test_boards_unequal_when_points_different(start_board):
    board_2 = Board.start()
    board_2.points = np.zeros((2,24), dtype=np.uint8)
    
    assert start_board != board_2

def test_boards_unequal_when_bar_different(start_board):
    board_2 = Board.start()
    board_2.bar = np.ones((2, ), dtype=np.uint8)
    
    assert start_board != board_2

def test_copied_boards_are_equal(start_board):
    board_copy = start_board.copy()

    assert start_board == board_copy

def test_board_inverted_twice_is_unchanged(start_board):
    board_inverted_twice = start_board.invert_view().invert_view()
    assert start_board == board_inverted_twice

def test_minimum_position_at_start(start_board):
    assert start_board.minpos() == 0

def test_all_in_home_board_is_false_at_start(start_board):
    assert not start_board.all_in_home_board()
    
def test_both_players_start_with_fifteen(start_board):
    assert start_board.num_left(player = 0) == 15
    assert start_board.num_left(player = 1) == 15

def test_bar_empty_at_start(start_board):
    assert start_board.bar_empty(player = 0)
    assert start_board.bar_empty(player = 0)

def test_cant_bear_off_at_start(start_board):
    assert not start_board.can_bear_off()

