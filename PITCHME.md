---?image=assets/img/kubernetes-logo.png
@title[TD-Gammon]
## TD-Gammon 

### Part 1 - Minikube and the first Pod

@snap[south docslink span-50]
[Gitlab](https://gitlab.com/drjroythorne/kubernetes-tutorial)
@snapend

+++?image=assets/img/bg/blue.jpg&position=left&size=25% 100%
@title[Install Minikube]

@snap[west text-white]
@size[1em](Setup)
@snapend

@snap[east span-70]
- Follow platform-specific guidelines from the [Kubernetes docs](https://kubernetes.io/docs/tasks/tools/install-minikube/) using [VirtualBox](https://www.virtualbox.org) as the Hypervisor.
@snapend

@snap[north-east template-note text-gray]
Minikube installation
@snapend

