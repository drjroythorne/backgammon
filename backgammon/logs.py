import logging
import sys

import structlog

# Setup jsonlogger to print JSON
handler = logging.StreamHandler(sys.stdout)

# Add both handler to logging
logging.basicConfig(
    format="%(message)s",
    handlers=[handler],
    level=logging.WARN
)

structlog.configure(
    processors=[
        structlog.contextvars.merge_contextvars,
        structlog.processors.add_log_level,
        structlog.processors.StackInfoRenderer(),
        structlog.dev.set_exc_info,
        structlog.processors.TimeStamper(),
        structlog.dev.ConsoleRenderer()
    ],
    wrapper_class=structlog.make_filtering_bound_logger(logging.WARN),
    context_class=dict,
    logger_factory=structlog.PrintLoggerFactory(),
    cache_logger_on_first_use=False
)
