import uuid

import numpy.random as npr
import structlog
import torch

from .strategies.estimator import (
    train_epochs,
    ValueFunctionEstimator,
    ValueFunctionEstimatorParams
)
from .features import Featurizer
from .game_state import GameState
from .game import roll
from . import logs


def argmax_policy(estimator):
    def _policy(next_states):
        values_states = (
           (estimator.evaluate_game_state(state=nxt), nxt)
           for nxt in next_states
        )
        return max(values_states, key=lambda x: x[0])[1]

    return _policy


def _random_policy(estimator):
    def _policy(next_states):
        # Just a random policy.
        return npr.choice(next_states, 1)[0]

    return _policy


def run_training_for_policy(logger):
    def _rewards(state: GameState):
        victor = state.victor()
        return {
            None: 0.0,
            0: 1.0,
            1: -1.0
        }[victor]

    def _state_transitions(current_state: GameState):
        r = roll()
        return current_state.next_states(rolls=r)

    def _estimator():
        estimator_params = ValueFunctionEstimatorParams(
            alpha=1e-2,
            gamma=0.95,
            lamb=0.7

        )
        return ValueFunctionEstimator(
            featurizer=Featurizer(),
            params=estimator_params
        )

    estimator = _estimator()
    _policy = argmax_policy(estimator)

    train_epochs(
        state_transitions=_state_transitions,
        policy=_policy,
        rewards=_rewards,
        start_state=GameState.start(start_player=0),
        epochs=100,
        estimator=estimator,
        logger=logger
    )

    return estimator


def main():
    logger = structlog.get_logger('train_policy')
    log = logger.bind(
        job_id=str(uuid.uuid4())
    )
    log.info('Training policy.')
    estimator = run_training_for_policy(logger=log)
    s = GameState.start()
    estimator.evaluate_game_state(s)
