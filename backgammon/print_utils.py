import crayons

def x_print():
    print(crayons.red(' x '), end='')

def o_print():
    print(crayons.white(' o '), end='')

def text_print(text):
    print(crayons.green(text), end='')
