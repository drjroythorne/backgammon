from functools import partial
import numpy as np


def _points_features(points):

    def gte_n_points(points, n):
        return (points >= n).astype(float).ravel()

    _1_points = partial(gte_n_points, 1)
    _2_points = partial(gte_n_points, 2)
    _3_points = partial(gte_n_points, 3)

    def gte4_points(x):
        return np.maximum(x.astype(float).ravel() - 3., 0.) / 2

    return np.concatenate([_1_points(points), _2_points(points), _3_points(points), gte4_points(points)])


def _bar_features(bar):
    return bar.astype(float) / 2


def _checkers_removed(board):
    return (15. - (np.sum(board.points, axis=1) + board.bar)) / 15.


def _current_player(player):
    return np.array([player == 0, player == 1])


class Featurizer:

    SIZE = 198
    DTYPE = np.float32

    def __call__(self, game_state):
        board = game_state.board

        return np.concatenate(
            [
                _points_features(points=board.points),
                _bar_features(bar=board.bar),
                _checkers_removed(board=board),
                _current_player(player=game_state.current_player)
            ]
        ).reshape(1, -1).astype(self.DTYPE)
    
