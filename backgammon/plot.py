import matplotlib.pyplot as plt

def plot_value_function(values):
    """
    :param np.array values: 2d numpy array of floats with shape (num_epochs, num_states)
    """

    plt.rcParams["figure.figsize"] = (30,20)

    num_epochs, num_states = values.shape

    for s in range(num_states):
        plt.plot(range(num_epochs), values[:, s], label=s)

    plt.legend()
