import numpy as np
import random

from .game_state import GameState
from .move_printer import print_move
from .strategies import strategies
from .print_utils import text_print

def decide_start_player():
    return random.choice([0,1]) 

def roll():
    return list(np.random.randint(1, 7, size=2))

def play(strategies):
        state = GameState.start(start_player = decide_start_player())
        while True:
            r = roll()
            print_move(state, r)
            nxt_states = state.next_states(r)
            state = strategies[state.current_player](nxt_states)
            victor = state.victor()
            if victor is not None:
                text_print(f'PLAYER {victor} WINS!\n')
                return victor
