from .print_utils import (
    x_print,
    o_print,
    text_print
)

def print_move(state, roll):
    state.board.pprint()

    text_print('Player: ')
    if state.current_player == 0:
        x_print()
    else:
        o_print()
    text_print(f'Roll: {roll}\n')
