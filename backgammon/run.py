import sys
import crayons
import click
import structlog
from tqdm import tqdm

from . import logs
from .game import play
from .strategies import player_strategies
from .print_utils import text_print

@click.command()
def main():
    logger = structlog.get_logger('backgammon')
    n_games = 1000
    n_victories = {0: 0, 1: 0}

    for n in tqdm(range(n_games)):
        logger.info('game', game_number=n)
        victor = play(player_strategies)
        n_victories[victor] += 1
        text_print(f'{n_victories}\n')
