from collections import namedtuple
import functools
import itertools

import numpy as np
import structlog

from .board import Board


class GameState:

    def __init__(self, board, current_player):
        self._logger = structlog.get_logger(__name__)

        self.board = board
        self.current_player = current_player

        self._logger.info('Initialised GameState.')

    @classmethod
    def start(cls, start_player = 0):
        return cls(
            board = Board.start(),
            current_player = start_player
        )

    def _next_player(self):
        return int(not self.current_player)

    def victor(self):
        """
        :return 0|1|None: When there is a victor, return the player identity (0 or 1), otherwise
        return None (when game is not complete).
        """

        current = self.current_player
        num_left_current = self.board.num_left(player = current)
        if num_left_current == 0:
            return current

        other = self._next_player()
        num_left_other = self.board.num_left(player = other)
        if num_left_other == 0:
            return other
        
        return None

    def _game_finished(self):
        return self.victor() is not None

    def _get_board_view(self):
        return self.board if self.current_player == 0 else self.board.invert_view()

    def _get_roll_permutations(self, rolls):
        if rolls[0] == rolls[1]: #thrown a double
            return [4*[rolls[0]]]
        return [rolls.copy(), list(reversed(rolls))]

    def _max_used_rolls(self, rolls):
        lengths, max_rolls = zip(*((len(r), max(r, default=-1)) for r in rolls))
        max_length = max(lengths)
        max_max_rolls = max(max_rolls)
        
        self._logger.info('max_length: {}'.format(max_length))
        self._logger.info('max_max_rolls: {}'.format(max_max_rolls))

        return max_length, lengths, max_rolls, max_max_rolls

    def next_states(self, rolls):
        if self._game_finished():
            return tuple()

        moves = self._next_moves(rolls) #list(list(tuple(source, target))
        #If no move available, return current board.
        if not moves:
            return (
                type(self)(
                    board = self.board,
                    current_player = self._next_player()
                ),
            )

        #Apply the moves
        #Flip the view for player 1
        board = self.board.invert_view() if self.current_player == 1 else self.board
        boards = [
            functools.reduce(self._apply_move, move, board)
            for move in moves
        ]
        if self.current_player == 1:
            boards = [board.invert_view() for board in boards]

        return tuple(
            type(self)(
                board = board,
                current_player = self._next_player()
            )
            for board in boards
        )

    def _next_moves(self, rolls):
        self._logger.info('Evaluating possible next_states from roll %s.', rolls)

        board = self._get_board_view()
        permutations = self._get_roll_permutations(rolls)

        nxt = list(itertools.chain.from_iterable(
            self._next_states(
                roll_sequence=r,
                current_states=[(board, tuple(), [])]
            ) for r in permutations
        ))

        self._logger.info('Possible next positions: \n %s', nxt)

        _, moves, used_rolls = zip(*nxt)

        max_length, lengths, max_rolls, max_max_rolls = self._max_used_rolls(used_rolls)

        #If no move available, return current board.
        if max_length == 0:
            return []

        # Filter the allowed new board states according to whether all available moves were made
        # and check that the largest roll was used if possible.
        return {move for move, length, max_roll in zip(moves, lengths, max_rolls) if length==max_length and max_roll == max_max_rolls}


    def _get_available_moves(self, board, roll):
        b = board.bar
        p = board.points

        if b[0] > 0: # Player has checkers on bar.
            if p[1, roll-1] < 2: # Empty point or blot
                return [(None, roll-1)]
            else: # No move available.
                return []

        else: # Nothing on the bar.
            self._logger.info('Nothing on the bar')
            current_positions = np.ravel(np.where(p[0,:]>0))

            if not current_positions.size: # Already won?
                return []  

            moves = []

            for i in current_positions:
                t = i + roll
                self._logger.info('Target %s (from roll %s, position %s)', t, roll, i)

                if t < 24:
                    self._logger.info('Target is on board')
                    if p[1,t] < 2: # Empty point or blot
                        new = board.copy()
                        new.points[0, i] -= 1
                        new.points[0, t] += 1
                        moves.append((i, t))
                        continue
                else: # Target is off board.
                    self._logger.info('Target %s is off board - check if can bear off.', t)
                    if board.can_bear_off() and (t == 24 or board.minpos() == i):
                        self._logger.info('Bearing off with exact match or from minimum position.')
                        moves.append((i, None))

            return moves

    def _apply_move(self, board, move):
        """
        :return list(Board)
        """
        b = board.bar
        p = board.points

        s, t = move
        if s is None: # Player has checkers on bar.
            if p[1, t] == 0: # No opponent checkers
                new = board.copy()
                new.bar[0] -= 1
                new.points[0, t] +=1
                return new

            elif p[1, t] == 1: # Blot.
                new = board.copy()
                new.bar[0] -= 1
                new.bar[1] += 1
                new.points[:, t] = np.array([1,0], dtype=np.uint8)
                return new

            else: # No move available.
                raise ValueError('Attempt to move to occupied point.')
                
        else: # Nothing on the bar.
            if t is None: # Target is off board, assuming can bear off
                new = board.copy()
                new.points[0, s] -= 1
                return new
            else: 
                if p[1,t] == 0: # No opponent checkers
                    new = board.copy()
                    new.points[0, s] -= 1
                    new.points[0, t] += 1
                    return new
                if p[1,t] == 1: # Blot.
                    new = board.copy()
                    new.points[0, s] -= 1
                    new.points[:, t] = np.array([1,0], dtype=np.uint8)
                    new.bar[1] += 1
                    return new

    def _next_states(self, roll_sequence, current_states):
        """
        :param list(int) roll_sequence: List of rolls remaining in turn.
        :param list(tuple(Board, list(tuple(source,target)), list(int)) current_states: List of tuples possible (intermediate) Board states
        , list of previous moves, and list of rolls used so far in turn.
        :return list(tuple(Board, list(int)):
        """

        if not roll_sequence:
            return current_states

        r = roll_sequence.pop()
        nxt = []

        for board, prv_moves, prv_rolls in current_states:
            nxt_moves = self._get_available_moves(board, r)

            if not nxt_moves:
                nxt.append((board.copy(), prv_moves, prv_rolls))
                continue

            nxt_boards = [self._apply_move(board, move) for move in nxt_moves]
            moves = [prv_moves+(move,) for move in nxt_moves]
            nxt_states = list(zip(nxt_boards, moves, itertools.repeat(prv_rolls + [r])))
            nxt.extend(nxt_states)

        return self._next_states(roll_sequence=roll_sequence, current_states=nxt)
