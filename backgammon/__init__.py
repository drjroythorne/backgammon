"""
backgammon - an implementation of Gerald Tesauro's "TD-Gammon" (Tesauro, Gerald (March 1995). "Temporal Difference Learning and TD-Gammon". Communications of the ACM. 38 (3). doi:10.1145/203330.203343. url:http://www.bkgm.com/articles/tesauro/tdl.html (Retrieved 21 April, 2019.) )
"""

VERSION = '0.0.3'
