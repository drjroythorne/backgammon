import structlog
import numpy as np

from .print_utils import (
    x_print,
    o_print,
    text_print
)

class Board:

    _logger = structlog.getLogger(__name__)

    def __init__(self, points, bar):
        self.points = points
        self.bar = bar

    @classmethod
    def start(cls):
        p = np.zeros((2, 24), dtype=np.uint8)
        p[0, 0], p[0, 11], p[0, 16], p[0, 18] = 2, 5, 3, 5
        p[1, 23], p[1, 12], p[1, 7], p[1, 5] = 2, 5, 3, 5
        bar = np.zeros((2, ), dtype=np.uint8)
        cls._logger.info('Board initialised start position.')
        return cls(points=p, bar=bar)

    def __repr__(self):
        return 'points: {}, bar: {}'.format(self.points, self.bar)

    def pprint(self):
        most_on_point = np.max(self.points)
        for i in range(most_on_point, 0, -1):
            for p in np.transpose(self.points):
                if p[0] >= i:
                    x_print()
                elif p[1] >= i:
                    o_print() 
                else:
                    text_print('   ')
            print()
        for i in range(24):
            text_print(str(i).ljust(3))

        text_print('\nBar:\n')
        for i in range(self.bar[0]):
            x_print()
        for i in range(self.bar[1]):
            o_print()
        print()

    def __hash__(self):
        return hash((hash(self.points.data.tobytes()),
                     hash(self.bar.data.tobytes())))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def copy(self):
        return Board(points=np.copy(self.points), bar=np.copy(self.bar))

    def invert_view(self):
        return Board(points=np.flip(self.points), bar=np.flip(self.bar))

    def minpos(self):
        min_location = np.min(np.where(self.points[0, :] > 0))
        self._logger.info('Furthest from freedom {}'.format(min_location))
        return min_location

    def all_in_home_board(self):
        return self.minpos() >= 18
        
    def num_left(self, player):
        return sum(self.points[player, :])

    def bar_empty(self, player):
        return self.bar[player] == 0

    def can_bear_off(self):
        bar_empty = self.bar_empty(player = 0)
        self._logger.info('bar_empty = {}'.format(bar_empty))
        all_in_home_board = self.all_in_home_board()
        self._logger.info('all_in_home_board = {}'.format(all_in_home_board))
        return all_in_home_board and bar_empty
