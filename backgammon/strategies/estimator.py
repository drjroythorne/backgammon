import json

import attr
import structlog
import torch
from tqdm import tqdm

device = torch.device('cpu')
dtype = torch.float
sig = torch.nn.Sigmoid()


@attr.s(auto_attribs=True, frozen=True)
class ValueFunctionEstimatorParams:
    alpha: float = 1e-2
    gamma: float = 0.95
    lamb: float = 0.7


class ValueFunctionEstimator(torch.nn.Module):

    def __init__(self, featurizer, params):
        """
        :param backgammon.features.Featurizer featurizer:
        :param ValueFunctionEstimatorParams
        """
        super().__init__()
        self._logger = structlog.get_logger(__name__)
        self._featurizer = featurizer
        self._params = params
        self._init_params()
        self._init_weights()
        self.init_epoch()

    def _init_params(self):
        self._alpha = self._params.alpha
        self._gamma = self._params.gamma
        self._gamma_lambda = self._params.gamma*self._params.lamb

    def _init_weights(self):
        D_in, H, D_out = self._featurizer.SIZE, 10, 1
        # Input layer weights
        self._w1 = torch.nn.Parameter(torch.randn(D_in, H, device=device, dtype=dtype, requires_grad=True))
        # Input layer bias
        self._b1 = torch.nn.Parameter(torch.randn(1, H, device=device, dtype=dtype, requires_grad=True))
        # Output layer weights
        self._w2 = torch.nn.Parameter(torch.randn(H, D_out, device=device, dtype=dtype, requires_grad=True))
        # Output layer bias
        self._b2 = torch.nn.Parameter(torch.randn(1, D_out, device=device, dtype=dtype, requires_grad=True))

    def init_epoch(self):
        self._etw1 = torch.zeros_like(self._w1).numpy()
        self._etb1 = torch.zeros_like(self._b1).numpy()
        self._etw2 = torch.zeros_like(self._w2).numpy()
        self._etb2 = torch.zeros_like(self._b2).numpy()

    def _eval_forward(self, state):
        return sig(torch.from_numpy(self._featurizer(state)).mm(self._w1) + self._b1).mm(self._w2) + self._b2

    def update(self, current_state, next_state_value, reward):
        v_cur = self._eval_forward(state=current_state)
        delta_sq = (self._gamma*next_state_value + reward - v_cur).pow(2).sum()
        delta_sq.backward()

        self._etw1 += self._w1.grad.numpy()
        self._etb1 += self._b1.grad.numpy()
        self._etw2 += self._w2.grad.numpy()
        self._etb2 += self._b2.grad.numpy()

        with torch.no_grad():
            self._w1 -= torch.from_numpy(self._alpha*self._etw1)
            self._b1 -= torch.from_numpy(self._alpha*self._etb1)
            self._w2 -= torch.from_numpy(self._alpha*self._etw2)
            self._b2 -= torch.from_numpy(self._alpha*self._etb2)

        self._w1.grad.zero_()
        self._b1.grad.zero_()
        self._w2.grad.zero_()
        self._b2.grad.zero_()

        self._etw1 = self._gamma_lambda*self._etw1
        self._etb1 = self._gamma_lambda*self._etb1
        self._etw2 = self._gamma_lambda*self._etw2
        self._etb2 = self._gamma_lambda*self._etb2

    def evaluate_game_state(self, state):
        with torch.no_grad():
            return self._eval_forward(state)

    def _weights_to_json(self):
        return {'w1': self._w1.mean().tolist()}


def _save_estimator(filename, estimator, epoch):
    state = {
        'epoch': epoch,
        'estimator': estimator.state_dict(),
        'params': estimator._params
    }
    torch.save(state, filename)


def train_epochs(state_transitions, policy, rewards, start_state, epochs, estimator, logger):
    """
    :param Callable state_transitions: Returns sequence of next available states.
    :param Callable policy:
    :param Callable rewards: Return reward for a given state transition.
    :param start_state:
    :param int epochs: Number of epochs.
    :param ValueFunctionEstimator estimator:
    :param structlog.generic.BoundLogger logger:
    """
    for epoch in tqdm(range(epochs)):
        estimator.init_epoch()
        cur = start_state
        next_states = state_transitions(cur)

        while(True):
            nxt = policy(next_states)
            reward = rewards(nxt)

            next_states = state_transitions(nxt)
            # Value function is zero for terminal states.
            if not next_states:
                estimator.update(
                    current_state=cur,
                    next_state_value=0.0,
                    reward=reward)
                break

            v_nxt = estimator.evaluate_game_state(state=nxt)
            estimator.update(
                current_state=cur,
                next_state_value=v_nxt,
                reward=reward
            )

            cur = nxt

        # Save every tenth epoch.
        if (epoch % 50) == 0:
            _save_estimator(
                filename='/tmp/backgammon_estimator_'+str(epoch),
                estimator=estimator,
                epoch=epoch
            )

        logger.info('weights', **estimator._weights_to_json())

    _save_estimator(
        filename='/tmp/backgammon_estimator_'+str(epochs),
        estimator=estimator,
        epoch=epochs
    )
