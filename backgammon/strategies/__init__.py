from functools import partial

from .strategies import prefer_to_be_together_strategy
from .strategies import prefer_to_be_together_and_capture_strategy

player_strategies = {
    0: partial(prefer_to_be_together_and_capture_strategy, player=0),
    1: partial(prefer_to_be_together_strategy, player=1)
}
