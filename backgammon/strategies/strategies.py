import numpy as np

def random_strategy(available_states, player):
    return random.choice(available_states)

def prefer_to_be_together_strategy(available_states, player):
    def num_alone(board, player):
        return sum(board.points[player,:] == 1)
    idx_least_loneliness = np.argmin(np.array([num_alone(s.board, player) for s in available_states]))
    return available_states[idx_least_loneliness]

def prefer_to_be_together_and_capture_strategy(available_states, player):
    def num_alone(board, player):
        return sum(board.points[player,:] == 1)

    def opponent_on_bar(board, player):
        return board.bar[int(not player)] > 0
        
    alone = np.array([num_alone(s.board, player) for s in available_states])
    idx_least_loneliness = np.where(alone == alone.min())[0]
    idx_on_bar = np.where(np.array([opponent_on_bar(s.board, player) for s in available_states]))[0]

    best = set(idx_least_loneliness).intersection(set(idx_on_bar))
    if best:
        return available_states[best.pop()]

    return available_states[idx_least_loneliness[0]]

