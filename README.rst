Backgammon
==========

backgammon - an implementation of Gerald Tesauro's "TD-Gammon" (Tesauro, Gerald (March 1995). "Temporal Difference Learning and TD-Gammon". Communications of the ACM. 38 (3). doi:10.1145/203330.203343. url:http://www.bkgm.com/articles/tesauro/tdl.html (Retrieved 21 April, 2019.) )


Contrubutions
-------------

Coding standards
~~~~~~~~~~~~~~~~

black

Packaging
~~~~~~~~~

bumpversion - https://github.com/c4urself/bump2version

Logging
~~~~~~~

td-agent-bit
~~~~~~~~~~~~
Running td-agent-bit service on Ubuntu host to receive Docker logs
(using fluentd logging driver)
https://fluentbit.io/documentation/0.13/installation/ubuntu.html


Training
~~~~~~~~

docker-compose up
